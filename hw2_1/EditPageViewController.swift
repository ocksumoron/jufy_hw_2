//
//  EditPageViewController.swift
//  hw2_1
//
//  Created by Даня on 18.11.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

import UIKit

class EditPageViewController: UIViewController {

    
    @IBOutlet weak var contactPhone: UITextField!
    @IBOutlet weak var contactName: UITextField!
    
    @IBAction func addContactDone(sender: AnyObject) {
        if (contactMgr.tochange == contactMgr.contacts.count) {
            contactMgr.addContact(contactName.text, phone: contactPhone.text)
        } else {
            contactMgr.contacts[contactMgr.tochange] = Contact(name: contactName.text, phone: contactPhone.text)
        }
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
