//
//  ViewController.swift
//  hw2_1
//
//  Created by Даня on 18.11.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var contacts = [Contact]()
    @IBOutlet weak var contactList: UITableView!
    
    @IBAction func addContactNew(sender: UIButton) {
        let editPageView = self.storyboard?.instantiateViewControllerWithIdentifier("editPage") as EditPageViewController
        contactMgr.tochange = contactMgr.contacts.count
        self.navigationController?.pushViewController(editPageView, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactMgr.contacts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "default")
        
        cell.textLabel.text = contactMgr.contacts[indexPath.row].name
        cell.detailTextLabel!.text = contactMgr.contacts[indexPath.row].phone
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let curContactInfo: Contact = Contact(name: contactMgr.contacts[indexPath.row].name, phone: contactMgr.contacts[indexPath.row].phone)
        
        
        var editPageView = self.storyboard!.instantiateViewControllerWithIdentifier("editPage") as EditPageViewController
        
        editPageView.loadView()
        
        contactMgr.tochange = indexPath.row
        editPageView.contactName.text = curContactInfo.name
        editPageView.contactPhone.text = curContactInfo.phone
        
        //self.navigationController!.pushViewController(editPageView, animated: true)
        //let vc = EditPageViewController() //change this to your class name
        self.presentViewController(editPageView, animated: true, completion: nil)
    }
}



