//
//  ContactManager.swift
//  hw2_1
//
//  Created by Даня on 18.11.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

import UIKit
import AddressBook

var contactMgr: ContactManager = ContactManager()

var addressBook: ABAddressBookRef?

class Contact {
    var name = "Default"
    var phone = "Default"
    init(name: String, phone: String) {
        self.name = name
        self.phone = phone
    }
}

class ContactManager: NSObject {
    var tochange: Int = 0
    var contacts = [Contact]()
    func addContact(name: String, phone: String) {
        contacts.append(Contact(name: name, phone: phone))
        tochange++
    }
    
    func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef? {
        if let ab = abRef {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        return nil
    }
    
    func loadContactList() {
        if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.NotDetermined) {
            var errorRef: Unmanaged<CFError>? = nil
            addressBook = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
            ABAddressBookRequestAccessWithCompletion(addressBook, { success, error in
                if success {
                    self.getContactNames()
                }
            })
        }
        if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Authorized) {
            self.getContactNames()
        }
    }
    
    func getContactNames() {
        var errorRef: Unmanaged<CFError>?
        addressBook = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        var contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
        for record:ABRecordRef in contactList {
            let contactPerson: ABRecordRef = record
            let contactName = ABRecordCopyCompositeName(contactPerson)?.takeRetainedValue()
            let contactPhone: ABMultiValueRef? = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)?.takeRetainedValue()
            var contactNameStr = ""
            var contactPhoneStr = ""
            if let contactName = contactName {
                contactNameStr = contactName as String
            }
            if let contactPhone = contactPhone {
                var contactPhoneWrapped = ABMultiValueCopyValueAtIndex(contactPhone, 0)?.takeRetainedValue()
                if let contactPhoneUnWrapped = contactPhoneWrapped {
                    contactPhoneStr = contactPhoneUnWrapped as String
                }
            }
            addContact(contactNameStr, phone: contactPhoneStr)
        }
    }
    
    override init() {
        super.init()
        loadContactList()
    }

}
